const algorithmMath = require('./algorithm-math')
const assert = require('assert')

describe('Algorithm tests', () => {
    test('Win statistics of two players with same ranking without rankingVariance is 50% without rankingVariance', () => {
        let winStatistics = algorithmMath.winStatistics({
            ranking: 1000,
            rankingVariance: 0
        }, {
            ranking: 1000,
            rankingVariance: 0
        })

        assert.strictEqual(winStatistics.chanceOfWinning, 0.5)
        assert.strictEqual(winStatistics.variance, 0)
    })

    test('Win statistics of first players with higher ranking without rankingVariance is more then 50% without rankingVariance', () => {
        let winStatistics = algorithmMath.winStatistics({
            ranking: 1373,
            rankingVariance: 0
        }, {
            ranking: 1000,
            rankingVariance: 0
        })

        assert.ok(winStatistics.chanceOfWinning > 0.5)
        assert.strictEqual(winStatistics.variance, 0)
    })

    test('Win chances are symmetrical', () => {
        let winStatistics1 = algorithmMath.winStatistics({
            ranking: 1373,
            rankingVariance: 0
        }, {
            ranking: 1000,
            rankingVariance: 0
        })

        let winStatistics2 = algorithmMath.winStatistics({
            ranking: 1000,
            rankingVariance: 0
        }, {
            ranking: 1373,
            rankingVariance: 0
        })

        assert.strictEqual(winStatistics1.chanceOfWinning, 1 - winStatistics2.chanceOfWinning)
        assert.strictEqual(winStatistics1.variance, 0)
        assert.strictEqual(winStatistics2.variance, 0)
    })

    test('Win rankingVariance of the ranking propagates to the result', () => {
        let winStatistics = algorithmMath.winStatistics({
            ranking: 1000,
            rankingVariance: 100
        }, {
            ranking: 1000,
            rankingVariance: 0
        })

        assert.strictEqual(winStatistics.chanceOfWinning, 0.5)
        assert.ok(winStatistics.variance > 0)
    })

    test('Win rankingVariance is symmetrical', () => {
        let winStatistics1 = algorithmMath.winStatistics({
            ranking: 1000,
            rankingVariance: 10
        }, {
            ranking: 1000,
            rankingVariance: 0
        })

        let winStatistics2 = algorithmMath.winStatistics({
            ranking: 1000,
            rankingVariance: 0
        }, {
            ranking: 1000,
            rankingVariance: 10
        })

        assert.strictEqual(winStatistics1.variance, winStatistics2.variance)
        assert.strictEqual(winStatistics1.chanceOfWinning, 0.5)
        assert.strictEqual(winStatistics2.chanceOfWinning, 0.5)
    })

    test('Win rankingVariance compounds', () => {
        let winStatistics1 = algorithmMath.winStatistics({
            ranking: 1000,
            rankingVariance: 10
        }, {
            ranking: 1000,
            rankingVariance: 0
        })

        let winStatistics2 = algorithmMath.winStatistics({
            ranking: 1000,
            rankingVariance: 10
        }, {
            ranking: 1000,
            rankingVariance: 10
        })

        assert.ok(winStatistics1.variance < winStatistics2.variance)
        assert.strictEqual(winStatistics1.chanceOfWinning, 0.5)
        assert.strictEqual(winStatistics2.chanceOfWinning, 0.5)
    })

    test('Win chance of 50% of uncertain rankings should be within one sigma', () => {
        let winStatistics = algorithmMath.winStatistics({
            ranking: 1000,
            rankingVariance: 100
        }, {
            ranking: 900,
            rankingVariance: 100
        })

        assert.ok(winStatistics.chanceOfWinning - winStatistics.variance < 0.5)
    })

    test('Win rankingVariance is lower if the scores are further apart', () => {
        let winStatistics1 = algorithmMath.winStatistics({
            ranking: 2000,
            rankingVariance: 100
        }, {
            ranking: 1000,
            rankingVariance: 100
        })

        let winStatistics2 = algorithmMath.winStatistics({
            ranking: 3000,
            rankingVariance: 100
        }, {
            ranking: 1000,
            rankingVariance: 100
        })
        assert.ok(winStatistics1.variance > winStatistics2.variance)
    })

    test('Win quality is 0 if the scores are equal', () => {
        let quality = algorithmMath.winQuality({
            ranking: 1000,
            rankingVariance: 100
        }, {
            ranking: 1000,
            rankingVariance: 50
        })

        assert.equal(quality, 0)
    })

    test('Win quality is less then one if scores are less then one sigma apart', () => {
        let quality = algorithmMath.winQuality({
            ranking: 1100,
            rankingVariance: 100
        }, {
            ranking: 1000,
            rankingVariance: 50
        })

        assert.ok(quality < 1)
    })
    test('Win quality is more then one if scores are more then one sigma apart', () => {
        let quality = algorithmMath.winQuality({
            ranking: 1200,
            rankingVariance: 100
        }, {
            ranking: 1000,
            rankingVariance: 50
        })

        assert.ok(quality > 1)
    })
})
