const algoMath = require('./algorithm-math')

/**
 * this would be a parameter of sorts, or could be automatically adjusted depending on player throughput.
 * lover value means better matches and more time taken to accept bad matches needs to be much less than qualityCutoff
 */
const worstAcceptableMatchQuality = 0.05
/**
 * if quality of the match is worse then this, dont even consider it. See ./doc/rating_and_variance.md
 */
const qualityCutoff = 3
/**
 * by this time the quality of even the worst acceptable match will reach 0
 */
const timeToConsiderWorstMatch = 60

/**
 * will try to create matches from queue every this many milliseconds
 */
const tryCreateMatchesInterval = 2000

/**
 * javascript-s single threadedness is working for us here (or against us). We do not need locks around our lists.
 * However this removes an important avenue of optimization, as this could be running on multiple threads.
 * If we still want to achieve that we need to create multiple instances of this service + a load balancer.
 */
class MatchmakingQueue {
    /**
     * @param {function} getCurrentTime accepts a function that returns the current time for easier mocking.
     */
    constructor(gameProxy, log) {
        this.log = log
        this.gameProxy = gameProxy
        this.getCurrentTime = function () { return new Date() }
        this.queue = {}
        this.matches = []
        this.looping = undefined
    }
    getAllPlayersFromQueue() {
        return Object.keys(this.queue).map((p) => this.convertToGetResult(this.queue[p]))
    }
    getPlayerFromQueue(playerId) {
        var player = this.queue[playerId]
        if (!player) return undefined
        return this.convertToGetResult(player)
    }
    addPlayerToQueue(playerWithRating) {
        // no validation against the schema is performed here. this is not visible outwards
        if (this.queue[playerWithRating.playerId]) {
            return
        }
        this.queue[playerWithRating.playerId] = { ...playerWithRating, addedToQueue: this.getCurrentTime() }
        // create matches with this new player
        for (const playerId of Object.keys(this.queue)) {
            if (playerId === playerWithRating.playerId) {
                // no matches with self
                continue
            }
            var player1 = playerWithRating
            var player2 = this.queue[playerId]

            var quality = algoMath.winQuality(player1, player2)
            if (quality > qualityCutoff) {
                // match is too poor, don't consider it
                continue
            }

            this.matches.push({
                playerIds: [player1.playerId, player2.playerId],
                quality,
                added: this.getCurrentTime(),
                totalTimeInQueue: this.timePassedSince(player2.addedToQueue)
            })
        }
    }
    removePlayerFromQueue(playerId) {
        delete this.queue[playerId]
        // delete all related matches info
        this.matches = this.matches.filter(m => m.playerIds[0] !== playerId && m.playerIds[1] !== playerId)
    }
    getGoodMatches() {
        var goodMatches = this.matches.filter(m => this.getCurrentMatchQuality(m) <= worstAcceptableMatchQuality)
        // this could contain multiple matches with the same people so we need to deduplicate them. but sort them first so the better ones stay
        let usedPlayers = new Set()
        goodMatches.sort((m) => m.quality)
        goodMatches = goodMatches.reduce((a, m) => {
            if (usedPlayers.has(m.playerIds[0]) || usedPlayers.has(m.playerIds[1])) {
                return a
            }
            usedPlayers.add(m.playerIds[0]).add(m.playerIds[1])
            a.push(m)
            return a
        }, [])

        return goodMatches.map((m) => m.playerIds)
    }
    getCurrentMatchQuality(match) {
        return Math.max(match.quality - qualityCutoff * (match.totalTimeInQueue + this.timePassedSince(match.added)) / timeToConsiderWorstMatch, 0)
    }
    convertToGetResult(player) {
        return {
            playerId: player.playerId,
            timeInQueue: this.timePassedSince(player.addedToQueue)
        }
    }
    /**
     * time passed since date in seconds
     * @param {Date} date
     */
    timePassedSince(date) {
        return ((this.getCurrentTime().getTime()) - date.getTime()) / 1000
    }
    /**
     * this is the function that will be periodically called to create matches, send to the game proxy and remove players
     */
    async loop() {
        var matches = this.getGoodMatches()
        // this is missing timeout and exception handles
        let playersToRemove = await Promise.all(matches.map((m) => {
            return this.gameProxy.tryStartGame(m)
        }))
        // flatten the array
        playersToRemove = playersToRemove.reduce((a, inner) => a.concat(inner), [])
        for (const playerToRemove of playersToRemove) {
            this.removePlayerFromQueue(playerToRemove)
        }
        if (this.log) {
            this.log(`Max wait time: ${Math.max(...(Object.keys(this.queue).map((p) => this.timePassedSince(this.queue[p].addedToQueue))))}`)
            this.log(`Players in queue: ${Object.keys(this.queue).length}`)
        }
    }
    startLoop() {
        this.looping = setInterval(this.loop.bind(this), tryCreateMatchesInterval)
    }
    dispose() {
        if (this.looping) clearInterval(this.looping)
    }
}

module.exports = MatchmakingQueue
