'use strict'
const Ajv = require('ajv')
var express = require('express')
var bodyParser = require('body-parser')

class MatchmakingService {
    constructor(matchmakingQueue, rankingProxy, log) {
        this.matchmakingQueue = matchmakingQueue
        this.rankingProxy = rankingProxy
        this.log = log

        var ajv = new Ajv({ schemaId: 'auto' })
        ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'))

        this.validateAddPlayerRequest = ajv.compile(require('./schemas/input_schema.json'))
        this.app = express()
        this.app.use(bodyParser.urlencoded({ extended: false }))
        this.app.use(bodyParser.json())

        this.app.post('/', this.addPlayer.bind(this))
        this.app.post('/cancel', this.removePlayer.bind(this))
        this.app.get('/', this.getAllPlayers.bind(this))
        this.app.get('/:playerId([\\d\\w]+)', this.getPlayer.bind(this))

        this.service = this.app.listen(3000, () => { })
        if (this.matchmakingQueue && this.matchmakingQueue.startLoop) this.matchmakingQueue.startLoop()
    }

    addPlayer(req, res, next) {
        try {
            let valid = this.validateAddPlayerRequest(req.body)
            if (!valid) {
                // log error
                if (this.log) this.log(`Schema validation errors: ${JSON.stringify(this.validateAddPlayerRequest.errors)}\nfor input: ${JSON.stringify(req.body)}`)
                res.status(400).send(this.validateAddPlayerRequest.errors)
                return next()
            }
            if (this.log) this.log(`Adding player ${req.body.playerId} to queue`)
            let playerWithRanking = this.rankingProxy.getRanking(req.body.playerId)
            this.matchmakingQueue.addPlayerToQueue(playerWithRanking)

            res.status(200).send('ok')
        } catch (error) {
            res.status(500).send(error)
        }
        return next()
    }
    removePlayer(req, res, next) {
        try {
            let valid = this.validateAddPlayerRequest(req.body)
            if (!valid) {
                // log error
                if (this.log) this.log(`Schema validation errors: ${JSON.stringify(this.validateAddPlayerRequest.errors)}\nfor input: ${JSON.stringify(req.body)}`)
                res.status(400).send(this.validateAddPlayerRequest.errors)
                return next()
            }
            if (this.log) this.log(`Removing player ${req.body.playerId} from queue`)
            this.matchmakingQueue.removePlayerFromQueue(req.body.playerId)

            res.status(200).send('ok')
        } catch (error) {
            res.status(500).send(error)
        }
        return next()
    }
    getAllPlayers(req, res, next) {
        try {
            if (this.log) this.log(`Getting players from queue`)
            let players = this.matchmakingQueue.getAllPlayersFromQueue()
            res.status(200).send(players)
        } catch (error) {
            res.status(500).send(error)
        }
        return next()
    }
    getPlayer(req, res, next) {
        try {
            if (this.log) this.log(`Getting player ${req.params.playerId} from queue`)
            let player = this.matchmakingQueue.getPlayerFromQueue(req.params.playerId)
            res.status(200).send(player)
        } catch (error) {
            res.status(500).send(error)
        }
        return next()
    }
    dispose() {
        this.service.close()
        if (this.matchmakingQueue && this.matchmakingQueue.dispose) this.matchmakingQueue.dispose()
    }
}

module.exports = MatchmakingService
