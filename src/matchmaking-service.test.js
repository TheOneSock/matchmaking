const request = require('supertest')
const MatchmakingService = require('./matchmaking-service')
const sinon = require('sinon')
const assert = require('assert')
const RankingProxyMock = require('./mocks/ranking-proxy')

var serviceWrapper

describe('Matchmaking service', () => {
    afterEach(() => {
        serviceWrapper.dispose()
    })

    describe('responds to GET /', () => {
        test('without fail', async () => {
            let queue = {
                getAllPlayersFromQueue: sinon.fake()
            }
            serviceWrapper = new MatchmakingService(queue)

            await request(serviceWrapper.service)
                .get('/')
                .expect(200)
        })

        test('by calling the internal queue once', async () => {
            let queue = {
                getAllPlayersFromQueue: sinon.fake()
            }
            serviceWrapper = new MatchmakingService(queue)
            await request(serviceWrapper.service)
                .get('/')
                .expect(200)

            assert.ok(queue.getAllPlayersFromQueue.calledOnce)
        })

        test('by calling the internal queue once and returns the data from it', async () => {
            let mockPlayersData = [{
                playerId: 'Tod',
                timeInQueue: 12
            },
            {
                playerId: 'Greg',
                timeInQueue: 15
            }]

            let queue = {
                getAllPlayersFromQueue: sinon.fake.returns(mockPlayersData)
            }
            serviceWrapper = new MatchmakingService(queue)
            let response = await request(serviceWrapper.service)
                .get('/')
                .expect(200)

            assert.deepEqual(mockPlayersData, response.body)

            assert.ok(queue.getAllPlayersFromQueue.calledOnce)
        })
    })

    describe('responds to GET /:playerId', () => {
        test('without fail', async () => {
            let playerId = 'Tod'
            let queue = {
                getPlayerFromQueue: sinon.fake()
            }
            serviceWrapper = new MatchmakingService(queue)

            await request(serviceWrapper.service)
                .get(`/${playerId}`)
                .expect(200)
        })
        test('by calling the internal queue once', async () => {
            let playerId = 'Tod'
            let queue = {
                getPlayerFromQueue: sinon.fake()
            }
            serviceWrapper = new MatchmakingService(queue)
            await request(serviceWrapper.service)
                .get(`/${playerId}`)
                .expect(200)

            assert.ok(queue.getPlayerFromQueue.calledOnce)
        })
        test('by calling the internal queue once and returns the data from it', async () => {
            let playerId = 'Tod'
            let mockPlayerData = {
                playerId,
                timeInQueue: 12
            }
            let queue = {
                getPlayerFromQueue: sinon.fake.returns(mockPlayerData)
            }
            serviceWrapper = new MatchmakingService(queue)
            let response = await request(serviceWrapper.service)
                .get(`/${playerId}`)
                .expect(200)

            assert.deepEqual(mockPlayerData, response.body)
            assert.ok(queue.getPlayerFromQueue.calledOnce)
        })
        test('by calling the internal queue once with the player id', async () => {
            let playerId = 'Tod'
            let mockPlayerData = {
                playerId,
                timeInQueue: 12
            }
            let queue = {
                getPlayerFromQueue: sinon.fake.returns(mockPlayerData)
            }
            serviceWrapper = new MatchmakingService(queue)
            let response = await request(serviceWrapper.service)
                .get(`/${playerId}`)
                .expect(200)

            assert.deepEqual(mockPlayerData, response.body)
            assert.ok(queue.getPlayerFromQueue.calledOnceWith(playerId))
        })
    })

    describe('responds to POST /', () => {
        test('with fail if post data not in correct format', async () => {
            serviceWrapper = new MatchmakingService()
            await request(serviceWrapper.service)
                .post('/')
                .send({ test: 'test' })
                .expect(400)
        })
        test('without fail if post data in correct format', async () => {
            let playerId = 'Tod'
            let queue = {
                addPlayerToQueue: sinon.fake()
            }
            serviceWrapper = new MatchmakingService(queue, new RankingProxyMock())

            await request(serviceWrapper.service)
                .post('/')
                .send({ playerId })
                .expect(200)
        })
        test('player ranking is fetched', async () => {
            let playerId = 'Tod'
            let queue = {
                addPlayerToQueue: sinon.fake()
            }
            let rankingProxyMock = new RankingProxyMock()
            serviceWrapper = new MatchmakingService(queue, rankingProxyMock)

            await request(serviceWrapper.service)
                .post('/')
                .send({ playerId })
                .expect(200)

            assert.ok(rankingProxyMock.getRanking.calledOnceWith(playerId))
        })

        test('player ranking is fetched and used to call internal queue', async () => {
            let playerRanking = {
                playerId: 'Tod',
                ranking: 1000,
                rankingVariance: 100
            }
            let queue = {
                addPlayerToQueue: sinon.fake()
            }
            let rankingProxyMock = new RankingProxyMock()
            rankingProxyMock.rankings[playerRanking.playerId] = playerRanking
            serviceWrapper = new MatchmakingService(queue, rankingProxyMock)

            await request(serviceWrapper.service)
                .post('/')
                .send({ playerId: playerRanking.playerId })
                .expect(200)

            assert.ok(rankingProxyMock.getRanking.calledOnceWith(playerRanking.playerId))
            assert.ok(queue.addPlayerToQueue.calledOnceWith(playerRanking))
        })
    })

    describe('responds to POST /cancel', () => {
        test('with fail if post data not in correct format', async () => {
            serviceWrapper = new MatchmakingService()
            await request(serviceWrapper.service)
                .post('/cancel')
                .send({ test: 'test' })
                .expect(400)
        })
        test('without fail if post data in correct format', async () => {
            let playerId = 'Tod'
            let queue = {
                removePlayerFromQueue: sinon.fake()
            }
            serviceWrapper = new MatchmakingService(queue)

            await request(serviceWrapper.service)
                .post('/cancel')
                .send({ playerId })
                .expect(200)
        })
        test('cancel request is forwarded to the queue', async () => {
            let playerId = 'Tod'
            let queue = {
                removePlayerFromQueue: sinon.fake()
            }
            serviceWrapper = new MatchmakingService(queue)

            await request(serviceWrapper.service)
                .post('/cancel')
                .send({ playerId })
                .expect(200)
            assert.ok(queue.removePlayerFromQueue.calledOnceWith(playerId))
        })
    })
})
