'use strict'
const Ajv = require('ajv')
const assert = require('assert')
const MatchmakingQueue = require('./matchmaking-queue')
const GameProxyMock = require('./mocks/game-proxy')

describe('Matchmaking queue', () => {
    describe('Basic functions', () => {
        test('adding to queue', async () => {
            const queue = new MatchmakingQueue()
            assert.doesNotThrow(() => queue.addPlayerToQueue({
                playerId: 'Tod',
                ranking: 1000,
                rankingVariance: 100
            }))
        })
        test('adding to queue and getting back returns correct data', async () => {
            const playerId = 'Tod'
            const queue = new MatchmakingQueue()
            var time = new Date()
            queue.getCurrentTime = () => new Date(time.getTime())
            const ajv = new Ajv({ schemaId: 'auto' })
            ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'))
            const schema = require('./schemas/player_matchmaking_info.json')

            queue.addPlayerToQueue({
                playerId: playerId,
                ranking: 1000,
                rankingVariance: 100
            })
            time.setSeconds(time.getSeconds() + 10)
            let queuePlayer = queue.getPlayerFromQueue(playerId)

            var valid = ajv.validate(schema, queuePlayer)
            assert.ok(valid, JSON.stringify(ajv.errors))
            assert.equal(queuePlayer.playerId, playerId)
            assert.equal(queuePlayer.timeInQueue, 10)
        })
        test('adding to queue and getting back a different user returns nothing', async () => {
            const playerId = 'Tod'
            const queue = new MatchmakingQueue()

            queue.addPlayerToQueue({
                playerId: playerId,
                ranking: 1000,
                rankingVariance: 100
            })

            let queuePlayer = queue.getPlayerFromQueue('Tod2')
            assert.ok(queuePlayer === undefined)
        })
        test('getAllPlayersFromQueue returns empty list if no players in queue', async () => {
            const queue = new MatchmakingQueue()
            let players = queue.getAllPlayersFromQueue()
            assert.deepEqual(players, [])
        })
        test('getAllPlayersFromQueue returns players added', async () => {
            const queue = new MatchmakingQueue()
            const players = [
                {
                    playerId: 'Tod',
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: 'Greg',
                    ranking: 1000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])
            let playersInQueue = queue.getAllPlayersFromQueue()
            assert.deepEqual(playersInQueue.map((p) => p.playerId), players.map((p) => p.playerId))
        })
        test('adding the same player to queue twice does list it twice, and keeps original wait time', async () => {
            const playerId = 'Tod'
            const player = {
                playerId: playerId,
                ranking: 1000,
                rankingVariance: 100
            }
            const queue = new MatchmakingQueue()
            var time = new Date()
            queue.getCurrentTime = () => new Date(time.getTime())

            queue.addPlayerToQueue(player)

            time.setSeconds(time.getSeconds() + 10)

            queue.addPlayerToQueue(player)

            let queuePlayers = queue.getAllPlayersFromQueue()
            assert.equal(queuePlayers.length, 1)
            assert.ok(queuePlayers[0].playerId, playerId)
            assert.equal(queuePlayers[0].timeInQueue, 10)
        })
        test('Remove Player not in queue does nothing', async () => {
            const queue = new MatchmakingQueue()
            assert.doesNotThrow(() => queue.removePlayerFromQueue('Tod'))
        })
        test('Remove Player in queue removes him/her', async () => {
            const queue = new MatchmakingQueue()
            const players = [
                {
                    playerId: 'Tod',
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: 'Greg',
                    ranking: 1000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])

            var countBeforeRemove = queue.getAllPlayersFromQueue().length
            queue.removePlayerFromQueue(players[0].playerId)
            var countAfterRemove = queue.getAllPlayersFromQueue().length
            assert.equal(countBeforeRemove - 1, countAfterRemove)
        })
    })
    describe('Matchmaking functions', () => {
        test('Two players of equal skill make a good match', async () => {
            var player1Id = 'Tod'
            var player2Id = 'Greg'
            const queue = new MatchmakingQueue()
            const players = [
                {
                    playerId: player1Id,
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: player2Id,
                    ranking: 1000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])

            var goodMatches = queue.getGoodMatches()
            assert.deepEqual(goodMatches[0].sort(), [player1Id, player2Id].sort())
        })
        test('Two players of similar skill make a good match after a while', async () => {
            var player1Id = 'Tod'
            var player2Id = 'Greg'
            const queue = new MatchmakingQueue()
            var time = new Date()
            queue.getCurrentTime = () => new Date(time.getTime())
            const players = [
                {
                    playerId: player1Id,
                    ranking: 1150,
                    rankingVariance: 100
                }, {
                    playerId: player2Id,
                    ranking: 1000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])

            var noMatches = queue.getGoodMatches()

            time.setSeconds(time.getSeconds() + 60)

            var goodMatches = queue.getGoodMatches()

            assert.equal(noMatches.length, 0)
            assert.deepEqual(goodMatches[0].sort(), [player1Id, player2Id].sort())
        })
        test('One player cannot be in two matches', async () => {
            const queue = new MatchmakingQueue()
            const players = [
                {
                    playerId: 'Tod',
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: 'Greg',
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: 'Sam',
                    ranking: 1000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])
            queue.addPlayerToQueue(players[2])

            var goodMatches = queue.getGoodMatches()
            assert.equal(goodMatches.length, 1)
        })
        test('Loop will take two players of equal skill and call the game proxy', async () => {
            var player1Id = 'Tod'
            var player2Id = 'Greg'
            const gameProxyMock = new GameProxyMock()
            const queue = new MatchmakingQueue(gameProxyMock)
            const players = [
                {
                    playerId: player1Id,
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: player2Id,
                    ranking: 1000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])

            await queue.loop()
            assert.ok(gameProxyMock.tryStartGame.calledWith([player2Id, player1Id]))
        })
        test('After game proxy is called with a match players will be removed', async () => {
            var player1Id = 'Tod'
            var player2Id = 'Greg'
            const gameProxyMock = new GameProxyMock()
            const queue = new MatchmakingQueue(gameProxyMock)
            const players = [
                {
                    playerId: player1Id,
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: player2Id,
                    ranking: 1000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])

            await queue.loop()

            var count = queue.getAllPlayersFromQueue().length
            assert.equal(count, 0)
        })
        test('Loop will create two games if two good matches', async () => {
            const gameProxyMock = new GameProxyMock()
            const queue = new MatchmakingQueue(gameProxyMock)
            const players = [
                {
                    playerId: 'Tod',
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: 'Greg',
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: 'Tom',
                    ranking: 2000,
                    rankingVariance: 100
                }, {
                    playerId: 'Sam',
                    ranking: 2000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])
            queue.addPlayerToQueue(players[2])
            queue.addPlayerToQueue(players[3])

            await queue.loop()
            assert.ok(gameProxyMock.tryStartGame.calledTwice)
        })
        test('Loop will remove only one player if one disconnected', async () => {
            var player1Id = 'Tod'
            var player2Id = 'Greg'
            const gameProxyMock = new GameProxyMock()
            gameProxyMock.disconnected.add(player2Id)
            const queue = new MatchmakingQueue(gameProxyMock)
            const players = [
                {
                    playerId: player1Id,
                    ranking: 1000,
                    rankingVariance: 100
                }, {
                    playerId: player2Id,
                    ranking: 1000,
                    rankingVariance: 100
                }]

            queue.addPlayerToQueue(players[0])
            queue.addPlayerToQueue(players[1])

            await queue.loop()

            var count = queue.getAllPlayersFromQueue().length
            assert.equal(count, 1)
        })
    })
})
