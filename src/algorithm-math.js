const s = 373
/**
 * Changes the fist player wins and the variance
 * see ./doc/rating_and_variance.md
 */
function winStatistics(playerRanking, vsPlayerRanking) {
    let e = Math.exp((vsPlayerRanking.ranking - playerRanking.ranking) / s)
    let se = 1 / (1 + e)
    let dse = e / (s * Math.pow(1 + e, 2))

    let vse = dse * Math.hypot(
        playerRanking.rankingVariance,
        vsPlayerRanking.rankingVariance)

    return {
        chanceOfWinning: se,
        variance: vse
    }
}
/**
 * How close the chance of winning is to 50% divided by the variance
 * the lower the better
 * see ./doc/rating_and_variance.md
 */
function winQuality(playerRanking, vsPlayerRanking) {
    let statistics = winStatistics(playerRanking, vsPlayerRanking)
    return Math.abs(statistics.chanceOfWinning - 0.5) / statistics.variance
}

module.exports = {
    winStatistics,
    winQuality
}
