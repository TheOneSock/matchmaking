const sinon = require('sinon')

/**
 * this proxy would call the actual game service to start the game.
 * if the game was able to start, both players are returned to be removed from the queue.
 * if game could not have been started the players (both, or just the disconnected one) wold be returned to be removed from the queue.
 */
class GameProxyMock {
    constructor(log) {
        this.disconnected = new Set()
        this.tryStartGame = sinon.fake(async (playerIds) => {
            if (log) log(`Trying to start game for ${JSON.stringify(playerIds)}.`)
            var disconnected = playerIds.filter(p => this.disconnected.has(p))
            if (disconnected.length) {
                return disconnected
            }
            return playerIds
        })
    }
}

module.exports = GameProxyMock
