const sinon = require('sinon')

/**
 * This proxy would be a wrapper around the ranking service calls. This mock gives you the ability to set the ranking before-hand.
 * If ranking is not set before-hand it is randomly chosen according to distribution described in ./doc/rating_and_variance.md
 */
class RankingProxyMock {
    constructor(log) {
        this.rankings = {}
        this.getRanking = sinon.fake((playerId) => {
            let ranking = this.rankings[playerId]
            if (ranking === undefined) {
                ranking = {
                    playerId: playerId,
                    ranking: getRandomRank(),
                    rankingVariance: getRandomVariance()
                }
                this.rankings[playerId] = ranking
            }
            if (log) log(`Got ranking ${JSON.stringify(ranking)}.`)
            return ranking
        })
    }
}
/**
 * see https://www.stata.com/statalist/archive/2005-08/msg00131.html
 */
function getRandomRank() {
    let r = Math.random()
    return Math.ceil(2319 - 373 * Math.log(1 / r - 1))
}

// Standard Normal variate using Box-Muller transform.
function getRandomVariance() {
    var u = 0
    var v = 0
    while (u === 0) u = Math.random()
    while (v === 0) v = Math.random()
    return Math.max(373 + 373 / 4 * Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v), 50)
}

module.exports = RankingProxyMock
