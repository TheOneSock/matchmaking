const MatchmakingService = require('./src/matchmaking-service')
const Queue = require('./src/matchmaking-queue')
const GameProxy = require('./src/mocks/game-proxy') // these would have been actual proxies and not mocks
const RankingProxy = require('./src/mocks/ranking-proxy') // these would have been actual proxies and not mocks

var gameProxy = new GameProxy()// new GameProxy(console.log)
var rankingProxy = new RankingProxy()// new RankingProxy(console.log)
var queue = new Queue(gameProxy, console.log)
/* eslint-disable */
var service = new MatchmakingService(queue, rankingProxy)//new MatchmakingService(queue, rankingProxy, console.log)
/* eslint-enable */

const fetch = require('node-fetch')

/**
 * in this demo there are a number of players joining per second and the matchmaking service is pairing them up.
 * The ranking for players is randomly generated from a logistic distribution.
 * You can add logging to each part of the code by un-commenting the constructors above
 * I let it run for quite a while with the current settings and the max wait time was around a minute max players in queue at once was around 40
 * with tweaking the parameters in './src/matchmaking-queue' you could achieve better or faster mapping according to flux of players. This could also be done in an automatic way
 */

const playersJoiningPerSecond = 100

setInterval(() => fetch('http://localhost:3000/', { method: 'POST', body: `{"playerId":"${Math.floor(Math.random() * 10000)}"}`, headers: { 'Content-Type': 'application/json' } }), 1000 / playersJoiningPerSecond)
