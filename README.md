# README

1. I decided that the service that provides the ranking and the service that will start the game between two players are separate services. (more on this [here](./doc/service_decription.md) )
2. The choices I made about how win chance is calculated and mow time affects which matches are chosen are described [here](./doc/algorithm.md) and [here](./doc/rating_and_variance.md)
3. to get the service started run **npm start**
4. to run the demo run **npm run demo**
5. the written assignment is [here](./doc/service_decription.md) )

## Code

1. The service is here: [matchmaking-service.js](./src/matchmaking-service.js)
2. the math part: calculating win chance, quality of the match... is here: [algorithm-math.js](./src/algorithm-math.js)
3. the queue (the part that ties it all together) is here: [matchmaking-queue.js](./src/matchmaking-queue.js)
4. tests are in **.test.js** in jest, and can be run with **npm test**
5. mocks for the two other services are in the /src/mocks folder

## Notes

1. Typescript would have been easier to read and understand, so I would have crated this assignment in ts-node if it wasn't explicitly mentioned to use javascript.
2. Sorry about the hand-drawn graphs. Time was short.
3. I may have spent too much time in researching the theory about matchmaking instead of coding, its been a while since I have done math so i got caught in the moment... I later realized that this is a coding assignment, not a research one
4. The documents are a bit of a mess due to time constraints
5. one major thing that is missing is logging

## Dependencies

- **express**: for creating the web server
- **body-parser**: to parse json-s with express post
- **ajv**: sor schema validations
- **node-fetch**

## Development dependencies

- **jest**: for testing
- **eslint**: to keep javascript code clean
- **supertest**: to unit test http server
- **sinon**: for easier mock creation
