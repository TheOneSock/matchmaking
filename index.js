const MatchmakingService = require('./src/matchmaking-service')
const Queue = require('./src/matchmaking-queue')
const GameProxy = require('./src/mocks/game-proxy') // these would have been actual proxies and not mocks
const RankingProxy = require('./src/mocks/ranking-proxy') // these would have been actual proxies and not mocks

var gameProxy = new GameProxy(console.log)
var rankingProxy = new RankingProxy(console.log)
var queue = new Queue(gameProxy, console.log)
/* eslint-disable */
var service = new MatchmakingService(queue, rankingProxy, console.log)
/* eslint-enable */
