# Player Rating and variance

Player rating is a number that is correlated to the chances of winning a game.

In theory if two players had the same rating they would on average win the same amount of time playing against each other.

The rating can potentially be measured independently for the following:

- **game map**. Certain players can be better on certain game maps
- **game type**. As in capture the flag, domination, death-match (of course this does not play a role in 1v1)
- **game**. The same matchmaking algorithm can be used for multiple different games (like mario kart and quake at the same time) in this case rating must be measured per game.
    If the games are similar at their core (unreal tournament and quake) the rating could be correlated.
- **Days since last played** as rating of a player generally degrades over time.

There is a number of different ways to convert a players rating into a number. The one taken here is the [MMR](https://dota2.gamepedia.com/Matchmaking_Rating) Which is very similar the [Elo rating system](https://en.wikipedia.org/wiki/Elo_rating_system).

Since there are no equations on the site we have to approximate or own with the data given. Firstly we can assume that the distribution of rating amongst players is a logistic curve as studies suggest.

The Logistic distribution
$$ f(x;\mu,s) = \frac{1}{s(e^{\frac{x-\mu}{2s}} +e^{-\frac{x-\mu}{2s}})} $$
Or more importantly for us its cumulative distribution function, the logistic function:
$$ F(x;\mu,s) = \frac{1}{1 +e^{-\frac{x-\mu}{s}}}$$

From the table on the dota 2 site their cumulative skill distribution is:

| Percentile | MMR  |
| ---------- | ---- |
| 5%         | 1100 |
| 10%        | 1500 |
| 25%        | 2000 |
| 50%        | 2250 |
| 75%        | 2731 |
| 90%        | 3200 |
| 95%        | 3900 |
| 99%        | 4100 |

Which means our parameters $\mu$ and $s$ are roughly $\mu = 2319, s = 373$ ([see fit](https://mycurvefit.com/index.html?action=openshare&id=5eb1cdd3-1a1b-452a-aae6-0abc239baf94])).

![Logistic curve fit](./assets/logistic_curve_fit.png)

Using the information from [here](https://en.wikipedia.org/wiki/Go_ranks_and_ratings#Elo-like_rating_systems_as_used_in_Go). The chance of a player A (with rating $R_A$) winning is:

$$S_E(A) = \frac{1}{1 + E};E = e^{\frac{R_B-R_A}{s}}$$

The [MMR](https://dota2.gamepedia.com/Matchmaking_Rating) page also mentions that "Players with high uncertainty are more likely to be matched with players that differ more greatly in skill level, and vice versa" we can account for that by using the variance formula to calculate the variance of the chance of winning:

$$\sigma S_E = \frac{E}{s(1 +E)^2}\sqrt{\sigma R_A^2 +\sigma R_B^2}$$

Which can influence the matcmaking. We could perhaps say that if the 50% chance of winning is within one sigma that is good enough to commence the match.