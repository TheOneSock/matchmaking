# Matchmaking service

At first glance there are two ways to design this microservice.

1. Service will need to call another service to get the player rating
2. Service will be called with the player rating already in the request

First approach means that the player id needs to be sent to this service which will then call the rating service to acquire the rating.

Second approach means that the request from the player (**C**) will first go trough another service (**?**) that will use the player id to get the rating (**R**), because you dont want to trust the players client to give you the right rating. This will then go to the matchmaking service (**M**).

![diagram1](./assets/service_comaprison.jpg)

The decision is not straightforward, so lets look at scaling.

In the first approach we can easily add more matchmaking services with either a load balancer, or maybe you want Asia on a different matchmaking service (with a different ranking service). If you wanted global rankings the ranking service would need a single instance ( or at least a single database). You could still split it into multiple servers by game mode, map,... but thats a different debate.

![diagram2](./assets/first_service.jpg)

Scaling the second approach seems a bit more difficult as load needs to be balanced from the service that prepares the ranking data (**?** lets call it data enrichment service) to the matchmaking service. On the other-hand if you need to scale the matchmaking service (which is not that computationally demanding) you probably also need another data enrichment service. This looks like they might need to be one service which brings us to the first approach.

![diagram3](./assets/second_service.jpg)

## Service methods

POST / :

add player to matchmaking queue

- input: [json](../src/schemas/input_schema.json)
- output: none/error

POST /cancel :

remove player from the matchmaking queue

- input: [json](../src/schemas/input_schema.json)
- output: none/error

GET / :

get all players in matchmaking

- output: array of [json](../src/schemas/player_matchmaking_info.json)

GET /{playerId}

- output: [json](../src/schemas/player_matchmaking_info.json)

## Communication with other services

Matchmaking service needs to communicate with the ranking service to get the player rank. And once it finds a match it needs to communicate the match to the "game" service which will initialize the game.

### Ranking service

Communication should be synchronous via one post method:

POST / :

- input: [json](../src/schemas/ranking_input_schema.json)
- output: [json](../src/schemas/ranking_output_schema.json)

### Game service

~~Communication should be asynchronous via something like RabbitMQ, Apache Kafka, Web Sockets...~~ Actually you need confirmation that the game service received and processed the request (game started) so you can remove the players out of the queue. One player could have disconnected already in that case you still want to remove him from the queue...

- input: [json](../src/schemas/game_input_schema.json)
- output: [json](../src/schemas/game_output_schema.json)

### Web Sockets

Communication to the ranking and game service could be done by Web sockets.

The main reason to do this on the Ranking service is speed as there is no http overhead.

For the game service the main reason would be that you could not need to wait for the game server to spin up a game and then respond. You could just send that a game needs to be started, temporarily remove the players from the queue and continue with matchmaking. Then readd the temporarily removed players if the game server sends that it cannot instantiate a game (or timeouts).

## Big Picture

For a wholesome matcmaking service ecosystem you would also need:

- single sign on for distributed systems ([reference](https://insready.com/en/blog/single-sign-using-oauth2-and-jwt-distributed-architecture))
- a load balancer in front of your multiple matchmaking services
- a good database for your ranking service like postgresql (other services do not need to persist their state)