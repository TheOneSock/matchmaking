# Matchmaking algorithm

The purpose of matchmaking is to take players willing to play and match them against other players of similar rating.

## Input

1. player id
2. player rating
3. player rating deviation/variance

Possible extensions:

1. party of players (doesn't matter in 1v1)
2. player clan (doesn't matter in 1v1)
3. player matchmaking preferences (much less useful in 1v1)
    1. prefer good match (more time waiting)
    2. prefer fast match (less time waiting)
4. number of games played
5. days since last played
6. daily rating (some days you can be better then others)

### Player Rating and variance

see [Rating and variance](rating_and_variance.md)

## Output

A pair of two players.

It is possible to also return their respective score and the game context, to save some time after the game, when you need this to update the player ratings.

## Implementation

The base idea is to match together players so that they have approximately equal chance to win. The calculation of probability to win will also return an uncertainty (described here [Rating and variance](rating_and_variance.md)). If 50% chance to win is within the uncertainty (one sigma) this is still a quite good. The players might not have the same ranking, but their performance varies enough that it is a good match.

One of the premises is that the player does not wait too long. This can be mitigated by increasing to also consider matches within 2 sigma after a while. And after that within 3 sigma. According to the table on this [site](https://en.wikipedia.org/wiki/68%E2%80%9395%E2%80%9399.7_rule) (also table below) if you play once daily you will win a 3 sigma match once per year. So i have chosen 3 sigma to be the maximum acceptable variance

|Range|Expected fraction of population inside range|Approximate expected frequency outside range|Approximate frequency for daily event|
|--|--|--|--|
|μ ± 0.5σ|0.382924922548026|2 in 3|Four times a week|
|μ ± σ|0.682689492137086|1 in 3|Twice a week|
|μ ± 1.5σ|0.866385597462284|1 in 7|Weekly|
|μ ± 2σ|0.954499736103642|1 in 22|Every three weeks|
|μ ± 2.5σ|0.987580669348448|1 in 81|Quarterly|
|μ ± 3σ|0.997300203936740|1 in 370|Yearly|
|μ ± 3.5σ|0.999534741841929|1 in 2149|Every six years|

Another way to prevent waiting too long is to just give up on matchmaking after a while and just take the closest match you can find. This is probably not desirable in ranked matches, but is totally fine in unranked. Matches with a large difference in ranking will have practically no effect on the players ranking unless the lower ranked player wins.

1. You have a list of players with ratings, variances and join times. Ordered by rating ascending (doesn't really matter). Insertion in such a list is $O(log(n))$
2. Every so often you do a pass through the list that creates matches. It prefers:
    1. prefers the win chance being closer to 50% mark in terms of variances
    2. higher wait times

The pass would take pairs of players ($O(n^2)$) calculate the quality of the match-up. The quality would take $W=(0.5-CalculatedProbabilityOfWinning)/uncertainty$ and $T = 3*(waitTimeOfBothPlayers/maxTimeOfWaiting)$ and reject anything with W > 3  then calculate the quality as $Q = W - T$. You would then take pairs that have Q < 0.5 or so. In words this would mean something like: if you wait the maximum amount of time you might get a match where you have only 0.3% of winning/loosing.

These parameters would have to be tweaked by more research and playtesting.

Optimization: you dont really have to recreate pairs every pass as W is constant and T depends only on the passage of time. so you can only create new pairs when adding a new player to the waiting list. This would change the algorithm to $O(n)$. You could also immediately create a match wile adding a player.